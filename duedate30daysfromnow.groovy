import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import java.sql.Timestamp;

int days = 30;

Calendar dueDate = Calendar.getInstance();

while (days > 0) {
    dueDate.add(Calendar.DAY_OF_YEAR, 1)
    --days;
}

/* Adjust for weekends -> set to following Monday */
int dow = dueDate.get(Calendar.DAY_OF_WEEK)

if (dow == 7) {
    dueDate.add(Calendar.DAY_OF_YEAR, 2)
} else if (dow == 1) {
    dueDate.add(Calendar.DAY_OF_YEAR, 1)
}

issue.setDueDate(new Timestamp(dueDate.getTimeInMillis()));