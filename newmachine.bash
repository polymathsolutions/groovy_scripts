#!/bin/bash

###
### Amazon Linux
###

# Variables
data=ebs            # Name of data volume; RHEL has /data already

# Change server time zone
# https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/set-time.html
# sudo nano /etc/sysconfig/clock
# Change to Australia/Sydney
# ZONE="Australia/Sydney" from ZONE="UTC"
sudo sed 's/UTC/Australia\/Sydney/' /etc/sysconfig/clock

### Install Amazon tools

# Systems manager agent
sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
# Cloudwatch dependencies
sudo yum install perl-Switch perl-DateTime perl-Sys-Syslog perl-LWP-Protocol-https
#
# Cloudwatch scripts
curl http://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.1.zip -O
unzip CloudWatchMonitoringScripts-1.2.1.zip
rm CloudWatchMonitoringScripts-1.2.1.zip
cd aws-scripts-mon
# Install AWS Inspector agent
wget https://d1wk0tztpsntt1.cloudfront.net/linux/latest/install
sudo bash install

# Check system configuration state
df
read -rsp $'Press Enter to continue\n' key

# Mount volume
sudo file -s /dev/xvdb
# Output should read:
# /dev/xvdb: data
sudo mkfs -t ext4 /dev/xvdb
sudo mkdir -m 000 /data
sudo mount /dev/xvdb /data

# Allow users to read/write new volume
sudo chown ec2-user /data

# Check system configuration state
df
read -rsp $'Press Enter to continue\n' key

# Reminder to modify fstab
echo "Remember to set up fstab so the volume mounts after reboot\n"
sudo cp /etc/fstab /etc/fstab.orig
echo "/etc/fstab has been backed up\n"
# http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-using-volumes.html
sudo file -s /dev/xvdb
ls -al /dev/disk/by-uuid
echo "\n"
read -rsp $'Write me down, then press Enter to continue\n' key

###
### 74fd6671-5e08-4fab-a9d5-af6f4a47c926
###

sudo nano /etc/fstab
read -rsp $'Press Enter to continue\n' key

###
### UUID=74fd6671-5e08-4fab-a9d5-af6f4a47c926       /data   ext4    defaults,nofail        0       2
###

# Reboot to ensure time zone change + hard drive map works
sudo reboot

# Install postgres
sudo yum install postgresql95 postgresql-server postgresql-contrib
sudo service postgresql initdb -D /data/pgdata
sudo nano /var/lib/pgsql9/data/pg_hba.conf

###
### local is for Unix domain socket connections only
### local   all     all         trust
### IPv4 local connection
### host    all     ec2-user    127.0.0.1/32    md5
### host    all     postgres    127.0.0.1/32    md5
### host    all     jiradbuser  127.0.0.1/32    md5
###

sudo nano /var/lib/pgsql9/data/postgresql.conf
sudo postgresql start
postgresql --version

# Edit config files to ensure
echo "Edit config files to ensure postgre looks in correct directory and is listening appropriately\n"
echo "File locations \ data_directory\n"
echo "Connections and authentication \ listen_address\n"
echo "Connections and authentication \ port\n"
echo "\n"


## Create database #
psql -U postgres -W
# CREATE USER jiradbuser WITH PASSWORD 'uqEUf0fhBzN1eynlFohS';
# CREATE DATABASE jiradb WITH ENCODING 'UNICODE' LC_COLLATE 'C' LC_CTYPE 'C' TEMPLATE template0;
# GRANT ALL PRIVILEGES ON DATABASE jiradb to jiradbuser;

# Ensure Postgresql starts on boot
# Add
# chkconfig postgresql on
### /usr/local/pgsql/bin/pg_ctl start -l logfile -D /usr/local/pgsql/data  # Ensure installation directory is correct + logfile writes to encrypted drive
# to /etc/rc.d/rc.local

# Download + install JIRA
wget https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-core-7.3.7-x64.bin
chmod a+x ./atlassian-jira-core-7.3.7-x64.bin

# Set password for jira user
sudo passwd jira

# Issue SSL
wget https://dl.eff.org/certbot-auto
chmod a+x certbot-auto
sudo ./certbot-auto --dry-run


## Harden environment
# Remove password for root
sudo passwd -l root