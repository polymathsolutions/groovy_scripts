import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import java.sql.Timestamp;

def customField =  ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Valid request received");

//if(customField != null) {
//    issue.getCustomFieldValue(customField);
//}
def valid = issue.getCustomFieldValue(customField);

Calendar dueDate = Calendar.getInstance();
dueDate.setTimeInMillis(valid.getTime());

int days = 30;
dueDate.add(Calendar.DAY_OF_YEAR, days);

/* Adjust for weekends -> set to following Monday */
int dow = dueDate.get(Calendar.DAY_OF_WEEK)

if (dow == 7) {
    dueDate.add(Calendar.DAY_OF_YEAR, 2)
} else if (dow == 1) {
    dueDate.add(Calendar.DAY_OF_YEAR, 1)
}

issue.setDueDate(new Timestamp(dueDate.getTimeInMillis()));