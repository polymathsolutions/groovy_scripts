import java.util.Date;
import java.util.Calendar;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.type.EventDispatchOption;
import java.sql.Timestamp;

/* Variables */
int days = 30;

/* Get valid request date */
def cfValid = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Valid request received");
def valid = issue.getCustomFieldValue(cfValid);

/* Add 30 days for extended requests */
def cfExtension = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Extension letter sent to applicant");
def extension = issue.getCustomFieldValue(cfExtension);
if (extension) {days += 30};

/* Add 30 days for third party consults */
def cfThirdParty = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Third party consultation letter issued");
def thirdParty = issue.getCustomFieldValue(cfThirdParty);
if (thirdParty) {days += 30};

log.warn("days (after adding extension(s)): " + days);

/* Adjust for consultation response */
def cfRCPsent = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Request for consultation process issued");
def rcpSent = issue.getCustomFieldValue(cfRCPsent);
def cfRCPresponse = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Consultation response received");
def rcpResponse = issue.getCustomFieldValue(cfRCPresponse);
if (rcpSent && rcpResponse) {days += (rcpResponse - rcpSent)};

log.warn("days (after adding xtn(s) + RCP): " + days);

/* Adjust for deposit request */
def cfADRsent = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Advance deposit request issued");
def adrSent = issue.getCustomFieldValue(cfADRsent);
def cfADRresponse = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Deposit request outcome finalised");
def adrResponse = issue.getCustomFieldValue(cfADRresponse);
if (adrSent && adrResponse) {days += (adrResponse - adrSent)};

log.warn("days (after adding xtn(s) + RCP + ADR): " + days);

/**
  * Calculate due date
  */
Calendar dueDate = Calendar.getInstance();
dueDate.setTimeInMillis(valid.getTime());
dueDate.add(Calendar.DAY_OF_YEAR, days);

/**
  * Adjust for weekends
  * If due date lands on a Sat/Sun -> set to following Monday
  */
int dow = dueDate.get(Calendar.DAY_OF_WEEK)

if (dow == Calendar.SATURDAY) {
    dueDate.add(Calendar.DAY_OF_YEAR, 2)
} else if (dow == Calendar.SUNDAY) {
    dueDate.add(Calendar.DAY_OF_YEAR, 1)
}

/**
  * Set the due date
  */
issue.setDueDate(new Timestamp(dueDate.getTimeInMillis()));