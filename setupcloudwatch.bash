#!/bin/bash
yum install perl-Switch perl-DateTime perl-Sys-Syslog perl-LWP-Protocol-https
curl http://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.1.zip -O
unzip CloudWatchMonitoringScripts-1.2.1.zip
rm CloudWatchMonitoringScripts-1.2.1.zip
cd aws-scripts-mon

# If you associated an AWS Identity and Access Management (IAM) role with your instance, verify that it grants permissions to perform the following operations:
#   cloudwatch:PutMetricData
#   cloudwatch:GetMetricStatistic
#   cloudwatch:ListMetrics
#   ec2:DescribeTags