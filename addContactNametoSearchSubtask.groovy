import com.atlassian.jira.issue.Issue
import com.atlassian.jira.component.ComponentAccessor;

Issue issue = issue;

/* Get contact name */
def cfContactName = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Contact name");
def contactName = issue.getCustomFieldValue(cfContactName);

/* Append to issue summary */
if (contactName) {issue.summary = "Internal Search - " + contactName};