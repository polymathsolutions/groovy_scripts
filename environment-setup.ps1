<#
## Set default settings
## Set client (user-input)
## Create keypair
## Create VPC
## Create Security Group
## Create VM
#>


## Set defaults ##

# Set Default AWS Credentials (ignore this if default credentials are already setup.)
Initialize-AWSDefaults -AccessKey 'AccessKey' -SecretKey 'SecretKey' -Region 'ap-southeast-2'

## Set customer ##

$customer = Read-Host "Please enter the customer acronym ";
$customerVerify = Read-Host "Please verify the customer acronym";

if ($customer -ne $customerVerify)
{
    Write-Host "ERROR 1: The customer acronyms do not match" -foregroundcolor Black -backgroundcolor Red;
    Write-Output "ERROR 1: The customer acronyms do not match";
    return 1;
}

# Set agency IPs
$agencyIPs = Read-Host "Please enter the customer acronym ";
$agencyIPsVerify = Read-Host "Please verify the customer acronym";


## Create keypair ##
#Create a KeyPair, this is used to encrypt the Administrator password.

# Folder path to copy the result keypair.pem file

$folder = "C:\keys\"

# Create the Key Pair
$keypair = New-EC2KeyPair -KeyName 'KeyPairName' -AccessKey 'Your Access Key' -SecretKey 'Your Secret Key' -Region 'ap-southeast-2'

# Write to the keypair.pem file
$keypairname = $customer + "keypair"
"$($keypair.KeyMaterial)" | out-file -encoding ascii -filepath $folder\$keypairname.pem
"KeyName: $($keypair.KeyName)" | out-file -encoding ascii -filepath $folder\$keypairname.pem -Append
"KeyFingerprint: $($keypair.KeyFingerprint)" | out-file -encoding ascii -filepath $folder\$keypairname.pem -Append

#The keypair.pem file is created in the $folder path.

<#
## The following script will create
## VPC 10.20.0.0/16, Subnet1 10.20.1.0/24, Subnet2 10.20.2.0/24
#>

## VPC ##

#Create new VPC
$vpcResult = New-EC2Vpc -CidrBlock '10.20.0.0/16'
$vpcId = $vpcResult.VpcId
Write-Output "VPC ID : $vpcId"

#Enable DNS Support & Hostnames in VPC
Edit-EC2VpcAttribute -VpcId $vpcId -EnableDnsSupport $true
Edit-EC2VpcAttribute -VpcId $vpcId -EnableDnsHostnames $true

#Create new Internet Gateway
$igwResult = New-EC2InternetGateway
$igwId = $igwResult.InternetGatewayId
Write-Output "Internet Gateway ID : $igwId"

#Attach Internet Gateway to VPC
Add-EC2InternetGateway -InternetGatewayId $igwId -VpcId $vpcId

#Create new Route Table
$rtResult = New-EC2RouteTable -VpcId $vpcId
$rtId = $rtResult.RouteTableId
Write-Output "Route Table ID : $rtId"

#Create new Route
$rResult = New-EC2Route -RouteTableId $rtId -GatewayId $igwId -DestinationCidrBlock '0.0.0.0/0'

#Create Subnet1 & associate route table
$sn1Result = New-EC2Subnet -VpcId $vpcId -CidrBlock '10.20.1.0/24' #-AvailabilityZone 'ap-southeast-1b'
$sn1Id = $sn1Result.SubnetId
Write-Output "Subnet1 ID : $sn1Id"
Register-EC2RouteTable -RouteTableId $rtId -SubnetId $sn1Id

#Create Subnet2 & associate route table
$sn2Result = New-EC2Subnet -VpcId $vpcId -CidrBlock '10.20.2.0/24' #-AvailabilityZone 'ap-southeast-1b'
$sn2Id = $sn2Result.SubnetId
Write-Output "Subnet2 ID : $sn2Id"
Register-EC2RouteTable -RouteTableId $rtId -SubnetId $sn2Id

Write-Output "VPC Setup Complete"

## Security Group ##

#Create a security group
$SecurityGroup = New-EC2SecurityGroup SampleGroup -Description "Security group 1" -AccessKey 'AccessKey' -SecretKey 'SecretKey' -Region 'ap-southeast-1'
Get-EC2SecurityGroup -GroupNames SampleGroup

#Add inbound rules
# Grant-EC2SecurityGroupIngress -GroupName SampleGroup -IpPermissions @{IpProtocol = "icmp"; FromPort = -1; ToPort = -1; IpRanges = @("0.0.0.0/0")}
# Grant-EC2SecurityGroupIngress -GroupName SampleGroup -IpPermissions @{IpProtocol = "tcp"; FromPort = 3389; ToPort = 3389; IpRanges = @("0.0.0.0/0")}
# Grant-EC2SecurityGroupIngress -GroupName SampleGroup -IpPermissions @{IpProtocol = "udp"; FromPort = 3389; ToPort = 3389; IpRanges = @("0.0.0.0/0")}
# Grant-EC2SecurityGroupIngress -GroupName SampleGroup -IpPermissions @{IpProtocol = "tcp"; FromPort = 5985; ToPort = 5986; IpRanges = @("0.0.0.0/0")}
Grant-EC2SecurityGroupIngress -GroupName SampleGroup -IpPermissions @{IpProtocol = "http"; FromPort = 80; ToPort = 80; IpRanges = @("0.0.0.0/0")}

#Add Outbound rules (this is optional)

Grant-EC2SecurityGroupEgress -GroupId $SecurityGroup -IpPermissions @{IpProtocol = "tcp"; FromPort = 5985; ToPort = 5986; IpRanges = @("0.0.0.0/0")}

Write-Output "Security Group Setup Complete"

## Create EC2 Instance ##

#Using Get-EC2Image

$ami = Get-EC2Image -Filters @{Name = "name"; Values = "Windows_Server-2012-RTM-English-64Bit-Base*"}
$imageid = $ami.ImageId

OR

#Using Get-EC2ImageByName

#use Get-EC2ImageByName to the list of images

Get-EC2ImageByName

#get the specific image
$ami = Get-EC2ImageByName -Names WINDOWS_2012_BASE
$imageid = $ami.ImageId

#Create VM

$result = New-EC2Instance -AccessKey 'AccessKey' -SecretKey 'SecretKey' -Region 'CurrentRegion' -ImageId $ImageId -MinCount 1 -MaxCount 1 -KeyName 'KeyPairName' -InstanceType 't1.micro'

$InstanceId = $result.Instances[0].InstanceId

# Create S3 bucket

