
###
###
### Table of Contents
###
###

### Set credentials + initialize AWS settings
### Setup VPC
### Setup Security Group
### Launch EC2 Instance

## Set credentials + initialize AWS settings

# Set profile name
set /P customer=Please enter the customer acronym 
set /P customerVerify=Please verify the customer acronym

if (%customer% == %customerVerify%)
{
    ##Write-Host "ERROR 1: The customer acronyms do not match" -foregroundcolor Black -backgroundcolor Red
    ##Write-Output "ERROR 1: The customer acronyms do not match"
    ##return 1
}


### Create security group

# Create security group
aws ec2 create-security-group --group-name devenv-sg --description "security group for development environment in EC2"
aws ec2 authorize-security-group-ingress --group-name devenv-sg --protocol tcp --port 22 --cidr 119.17.55.238/32
aws ec2 authorize-security-group-ingress --group-name devenv-sg --protocol tcp --port 80 --cidr 119.17.55.238/32
aws ec2 authorize-security-group-ingress --group-name devenv-sg --protocol tcp --port 443 --cidr 119.17.55.238/32

# Create key pair
aws ec2 create-key-pair --key-name devenv-key --query "KeyMaterial" --output text > devenv-key.pem