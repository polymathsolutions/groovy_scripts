

Write-Output "Hello!";

$customer = Read-Host "Please enter the customer acronym ";
$customerVerify = Read-Host "Please verify the customer acronym";

if ($customer -ne $customerVerify)
{
    Write-Host "ERROR 1: The customer acronyms do not match" -foregroundcolor Black -backgroundcolor Red;
    Write-Output "ERROR 1: The customer acronyms do not match";
    return 1;
}

Write-Output "";
Write-Output "Commencing configuration with $customer";
Write-Output "";

$keypairname = $customer + "keypair"

Write-Output $keypairname