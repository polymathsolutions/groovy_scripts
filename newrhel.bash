#!/bin/bash

###
### RHEL7
###

# Variables
data=ebs            # Name of data volume; RHEL has /data already

# Change server time zone
# https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System_Administrators_Guide/chap-Configuring_the_Date_and_Time.html
# 2.1.4 Changing the Time Zone
timedatectl set-timezone Australia/Sydney



### Install Amazon tools

# Systems manager agent
sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
# Cloudwatch dependencies
sudo yum install perl-Switch perl-DateTime perl-Sys-Syslog perl-LWP-Protocol-https perl-Digest-SHA -y
sudo yum install zip unzip
# Cloudwatch scripts
curl http://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.1.zip -O
unzip CloudWatchMonitoringScripts-1.2.1.zip
rm CloudWatchMonitoringScripts-1.2.1.zip
cd aws-scripts-mon
# Install AWS Inspector agent
wget https://d1wk0tztpsntt1.cloudfront.net/linux/latest/install
sudo bash install

# Check system configuration state
df
read -rsp $'Press Enter to continue\n' key

# Mount volume
sudo file -s /dev/xvdb
# Output should read:
# /dev/xvdb: data
sudo mkfs -t ext4 /dev/xvdb
sudo mkdir -m 000 /$data
sudo mount /dev/xvdb /$data

# Allow users to read/write new volume
sudo chown ec2-user /$data

# Check system configuration state
df
read -rsp $'Press Enter to continue\n' key

# Reminder to modify fstab
echo "Remember to set up fstab so the volume mounts after reboot\n"
sudo cp /etc/fstab /etc/fstab.orig
echo "/etc/fstab has been backed up\n"
# http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-using-volumes.html
sudo file -s /dev/xvdb
ls -al /dev/disk/by-uuid
echo "\n"
read -rsp $'Write me down, then press Enter to continue\n' key

###
### e43f5db9-76bc-4192-b7e7-7e1375e831a7
###

sudo vi /etc/fstab
sudo mount -a
read -rsp $'Press Enter to continue\n' key

###
### UUID=74fd6671-5e08-4fab-a9d5-af6f4a47c926       /data   ext4    defaults,nofail        0       2
###

# Reboot to ensure time zone change + hard drive map works
sudo reboot

# Install postgres
sudo yum install https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7-x86_64/pgdg-redhat96-9.6-3.noarch.rpm
# sudo yum list postgre*
sudo yum install postgresql96.x86_64 postgresql96-libs.x86_64 postgresql96-server.x86_64
sudo service postgresql-setup initdb -D /$data/pgdata
sudo vi /var/lib/pgsql/data/pg_hba.conf

###
### local is for Unix domain socket connections only
### local   all     all         trust
### IPv4 local connection
### host    all     ec2-user    127.0.0.1/32    md5
### host    all     postgres    127.0.0.1/32    md5
### host    all     jiradbuser  127.0.0.1/32    md5
###

sudo vi /var/lib/pgsql/data/postgresql.conf

###
### listen_addresses = 'localhost'
### port = 5432
###

# Edit config files to ensure
echo "Edit config files to ensure postgre looks in correct directory and is listening appropriately\n"
echo "File locations \ data_directory\n"
echo "Connections and authentication \ listen_address\n"
echo "Connections and authentication \ port\n"
echo "\n"

sudo service postgresql start


## Create database #
psql -U postgres -W
# CREATE USER jiradbuser WITH PASSWORD 'uqEUf0fhBzN1eynlFohS';
# CREATE DATABASE jiradb WITH ENCODING 'UNICODE' LC_COLLATE 'C' LC_CTYPE 'C' TEMPLATE template0;
# GRANT ALL PRIVILEGES ON DATABASE jiradb to jiradbuser;

# Ensure Postgresql starts on boot
# Add
# chkconfig postgresql on
### /usr/local/pgsql/bin/pg_ctl start -l logfile -D /usr/local/pgsql/data  # Ensure installation directory is correct + logfile writes to encrypted drive
# to /etc/rc.d/rc.local

# Download + install JIRA
wget https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-core-7.3.7-x64.bin && chmod a+x ./atlassian-jira-core-7.3.7-x64.bin

# Set password for jira user
sudo passwd jira

# Issue SSL
wget https://dl.eff.org/certbot-auto && chmod a+x certbot-auto
sudo ./certbot-auto --dry-run


## Harden environment
# Remove password for root (disable local root access)
sudo passwd -l root

# Disable password based remote root login
sudo vi /etc/ssh/sshd_config
###
### from:
###      #PermitRootLogin yes
### to:
###      PermitRootLogin without-password