#!/bin/bash

###
### Ubuntu Server
###

# Variables
datavolname=ebs                                                                 # Name of data volume; RHEL has /data already
user=ubuntu
domain=client.dash.polysol.com.au
certdir=/etc/letsencrypt/live/$domain/
keytooldir=/opt/atlassian/jira/jre/bin/ #java keytool located in jre/bin        # Update to adjust for install location
myemail=itsa+$domain@polysol.com.au
networkdevice=eth0                                                              # Network device  (run ifconfig to get the name)
keystoredir=/home/jira/.keystore                                                # Located in home dir of user that you Tomcat is running under - just replace jira with your user you use for Tomcat, see ps -ef to get user name if you do not know



# Change server time zone
sudo dpkg-reconfigure tzdata





### Install Amazon tools

# Systems manager agent
cd /tmp			
wget https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/debian_amd64/amazon-ssm-agent.deb
sudo dpkg -i amazon-ssm-agent.deb
sudo systemctl enable amazon-ssm-agent
# Cloudwatch dependencies
sudo apt-get update
sudo apt-get install unzip
sudo apt-get install libwww-perl libdatetime-perl
# Cloudwatch scripts
curl http://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.1.zip -O
unzip CloudWatchMonitoringScripts-1.2.1.zip
rm CloudWatchMonitoringScripts-1.2.1.zip
cd aws-scripts-mon
cp awscreds.template awscreds.conf
nano awscreds.conf
# Install AWS Inspector agent
wget https://d1wk0tztpsntt1.cloudfront.net/linux/latest/install
sudo bash install

### Mount hard drive

# Check system configuration state
df
lsblk
read -rsp $'Press Enter to continue\n' key

# Mount volume
sudo file -s /dev/xvdf
# Output should read:
# /dev/xvdb: data
# This means the volume is empty and OK to format
sudo mkfs -t ext4 /dev/xvdf
sudo mkdir -m 000 /$datavolname
sudo mount /dev/xvdf /$datavolname

# Allow users to read/write new volume
sudo chown $user /$datavolname

# Check system configuration state
df
read -rsp $'Press Enter to continue\n' key

# Modify fstab
echo "Remember to set up fstab so the volume mounts after reboot\n"
sudo cp /etc/fstab /etc/fstab.orig
echo "/etc/fstab has been backed up\n"
# http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-using-volumes.html
# https://devopscube.com/mount-ebs-volume-ec2-instance/
sudo file -s /dev/xvdf
ls -al /dev/disk/by-uuid
echo "\n"
read -rsp $'Write me down, then press Enter to continue\n' key

###
### e7699dd4-ab5e-4aec-b8cf-29ee4d414e03
###

sudo nano /etc/fstab
sudo mount -a
### If no errors returned = good
read -rsp $'Press Enter to continue\n' key

###
### UUID=74fd6671-5e08-4fab-a9d5-af6f4a47c926       /data   ext4    defaults,nofail        0       2
###
### nobootwait is Debian/Ubuntu specific as per http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-using-volumes.html

# Install postgres
sudo apt-get install postgresql
cd /etc/postgresql/9.5/main
./initdb -D /$datavolname/pgdata
sudo nano /data/pgdata/pg_hba.conf

## Create database #
su postgres
psql -U postgres -W
# CREATE USER jiradbuser WITH PASSWORD 'uqEUf0fhBzN1eynlFohS';
# CREATE DATABASE jiradb WITH ENCODING 'UNICODE' LC_COLLATE 'C' LC_CTYPE 'C' TEMPLATE template0;
# GRANT ALL PRIVILEGES ON DATABASE jiradb to jiradbuser;
# \q
exit

# Make new directory
mkdir -p /data/pgdata
sudo chown -R postgres:postgres /database

sudo passwd postgres
su postgres
/usr/lib/postgresql/9.5/bin/initdb -D /database
exit
sudo systemctl stop postgresql
sudo systemctl start postgresql

###
### local is for Unix domain socket connections only
### local   all     all         trust
### IPv4 local connection
### host    all     ec2-user    127.0.0.1/32    md5
### host    all     postgres    127.0.0.1/32    md5
### host    jiradb  jiradbuser  127.0.0.1/32    md5
###

cd /data/pgdata
sudo nano pg_hba.conf
sudo nano postgresql.conf

###
### listen_addresses = 'localhost'
### port = 5432
###

# Edit config files to ensure
echo "Edit config files to ensure postgre looks in correct directory and is listening appropriately\n"
echo "File locations \ data_directory\n"
echo "Connections and authentication \ listen_address\n"
echo "Connections and authentication \ port\n"
echo "\n"

# Ensure Postgresql starts on boot
sudo systemctl enable postgresql



# Reboot to ensure time zone change + hard drive map works
sudo reboot

# Download + install JIRA
wget https://www.atlassian.com/software/jira/downloads/binary/atlassian-servicedesk-3.6.0-x64.bin
chmod a+x ./atlassian-servicedesk-3.6.0-x64.bin
sudo ./atlassian-servicedesk-3.6.0-x64.bin

# Set password for jira user
sudo passwd jira

                                    # Issue SSL
                                    sudo apt-get update
                                    sudo apt-get install software-properties-common
                                    sudo add-apt-repository ppa:certbot/certbot
                                    sudo apt-get update
                                    sudo apt-get install python-certbot-apache

                                    sudo certbot --apache


### Issue SSL
mkdir -p /var/git/letsencrypt
cd /var/git
git clone https://github.com/letsencrypt/letsencrypt
cd letsencrypt
# git pull origin master
iptables -I INPUT -p tcp -m tcp --dport 9999 -j ACCEPT
iptables -t nat -I PREROUTING -i $networkdevice -p tcp -m tcp --dport 80 -j REDIRECT --to-ports 9999

./letsencrypt-auto certonly --standalone --test-cert --break-my-certs -d $mydomain --standalone-supported-challenges http-01 --http-01-port 9999 --renew-by-default --email $myemail --agree-tos
#./letsencrypt-auto certonly --standalone -d $mydomain --standalone-supported-challenges http-01 --http-01-port 9999 --renew-by-default --email $myemail --agree-tos

### --standalone-supported-challenges is now deprecated; using --preferred-challenges
./letsencrypt-auto certonly --standalone --test-cert --break-my-certs -d $mydomain --preferred-challenges http-01 --http-01-port 9999 --renew-by-default --email $myemail --agree-tos
#./letsencrypt-auto certonly --standalone -d $mydomain --preferred-challenges http-01 --http-01-port 9999 --renew-by-default --email $myemail --agree-tos

iptables -t nat -D PREROUTING -i $networkdevice -p tcp -m tcp --dport 80 -j REDIRECT --to-ports 9999
iptables -D INPUT -p tcp -m tcp --dport 9999 -j ACCEPT

$keytooldir/keytool -delete -alias root -storepass changeit -keystore $keystoredir
$keytooldir/keytool -delete -alias tomcat -storepass changeit -keystore $keystoredir

openssl pkcs12 -export -in $certdir/fullchain.pem -inkey $certdir/privkey.pem -out $certdir/cert_and_key.p12 -name tomcat -CAfile $certdir/chain.pem -caname root -password pass:aaa.k

$keytooldir/keytool -importkeystore -srcstorepass aaa -deststorepass changeit -destkeypass changeit -srckeystore $certdir/cert_and_key.p12 -srcstoretype PKCS12 -alias tomcat -keystore $keystoredir
$keytooldir/keytool -import -trustcacerts -alias root -deststorepass changeit -file $certdir/chain.pem -noprompt -keystore $keystoredir


# Restart Tomcat server (running JIRA)
service jira stop
service jira start




### Harden environment
# Remove password for root (disable local root access)
sudo passwd -l root

# Disable remote root login (password based or otherwise)
sudo nano /etc/ssh/sshd_config
###
### Ensure PermitRootLogin line reads
###      PermitRootLogin no


### Add 2FA

sudo apt-get update
sudo apt-get install libpam-google-authenticator
nano /etc/pam.d/common-auth
###
### Look for:
### auth    [success=1 default=ignore]      pam_unix.so nullok_secure
###
### Above this, add line:
### auth required pam_google_authenticator.so nullok
###

google-authenticator


sudo nano /etc/pam.d/sshd

### Once all useres have an OATH-TOTP key, nullok can be removed from /etc/pam.d/sshd

### REFERENCES
### 
### 2FA
### https://www.digitalocean.com/community/tutorials/how-to-set-up-multi-factor-authentication-for-ssh-on-ubuntu-14-04
### https://medium.com/aws-activate-startup-blog/securing-ssh-to-amazon-ec2-linux-hosts-18e9b72319d4
### https://www.linux.com/learn/how-set-2-factor-authentication-login-and-sudo
###
### DATABASE
### http://climber2002.github.io/blog/2015/02/07/install-and-configure-postgresql-on-ubuntu-14-dot-04/


### TROUBLESHOOTING
###
### Fix "Authentication Token Manipulation Error" When Changing User Password In Ubuntu
###
### According to:
### http://www.codevoila.com/post/26/fix-authentication-token-manipulation-error-when-changing-user-password-in-ubuntu
### Run:
###     mount -rw -o remount /
### then:
###     ls -l /etc/shadow
### Ensure:
### -rw-r----- 1 root shadow 1025 Feb  11 22:11 /etc/shadow
### or similar is returned (write permission on /etc/shadow)