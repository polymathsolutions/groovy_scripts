﻿#Set-ExecutionPolicy -ExecutionPolicy Bypass -Scope CurrentUser
#Set-ExecutionPolicy remotesigned

<##
### Table of Contents
##>

### Set defaults + initialize AWS settings
### Setup VPC
### Setup Security Group
### Launch EC2 Instance
### Create S3 bucket

## Set defaults + initialize AWS settings

# Set customer name
Write-Output "Hello!"

$customer = Read-Host "Please enter the customer acronym "
$customerVerify = Read-Host "Please verify the customer acronym"

if ($customer -ne $customerVerify)
{
    Write-Host "ERROR 1: The customer acronyms do not match" -foregroundcolor Black -backgroundcolor Red
    Write-Output "ERROR 1: The customer acronyms do not match"
    return 1
}

$customerIP = Read-Host "Please enter the customer IPs "
$customerIPVerify = Read-Host "Please verify the customer IPs"

if ($customerIP -ne $customerIPVerify)
{
    Write-Host "ERROR 2: The customer IP addresses do not match" -foregroundcolor Black -backgroundcolor Red
    Write-Output "ERROR 2: The customer addresses do not match"
    return 2
}

Write-Output ""
Write-Output "Commencing configuration with $customer at $customerIP"
Write-Output ""

# Create key pair
$customerKp = New-EC2KeyPair -KeyName "$customer-key-pair"
$customerKp.KeyMaterial | Out-File C:\Users\jvan\.aws\$customer-key-pair.pem
Write-Output "Key pair created at C:\Users\jvan\.aws\$customer-key-pair.pem"

# Set credentials
#Set-AWSCredentials -AccessKey {AKIAIOSFODNN7EXAMPLE} -SecretKey {wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY} -StoreAs $profile
#Get-AWSCredentials

# Set IP addresses for security filters (CIDR format)
$wotsoNthStrIP = "119.17.55.238/22"
$whitelistIPs = $wotsoNthStrIP + "," + $customerIP

# Initialize AWS settings
#Initialize-AWSDefaults -ProfileName $profile -Region ap-southeast-2
Initialize-AWSDefaults -Region ap-southeast-2
$availabilityZone = ap-southeast-2a

# Set tags
$Tags = @( @{key="Customer"; value="$customer"}; ` # change the last ; to , if this doesn't work JV
           @{key="Stack"; value="Production"} )

# Set year
$year = Get-Date -Format "yyyy"

## Setup VPC

# Create new VPC
$vpcResult = New-EC2Vpc -CidrBlock '10.20.0.0/28'
$vpcId = $vpcResult.VpcId
Write-Output "VPC ID: $vpcId"

# Enable DNS Support & Hostnames in VPC
Edit-EC2VpcAttribute -VpcId $vpcId -EnableDnsSupport $true
Edit-EC2VpcAttribute -VpcId $vpcId -EnableDnsHostnames $true

# Create new Internet Gateway
$igwResult = New-EC2InternetGateway
$igwId = $igwResult.InternetGatewayId
Write-Output "Internet Gateway ID: $igwId"
Get-EC2InternetGatway -InternetGatewayId $igwId

# Attach Internet Gateway to VPC
Add-EC2InternetGateway -InternetGatewayId $igwId -VpcId $vpcId

# Create new Route Table
$rtTblResult = New-EC2RouteTable -VpcId $vpcId
$rtTblId = $rtTblResult.RouteTableId
Write-Output "Route Table ID : $rtTblId"

# Create new Route
$rtResult = New-EC2Route -RouteTableId $rtTblId -GatewayId $igwId -DestinationCidrBlock '0.0.0.0/0'

Write-Output "Route added : $rtResult"
Get-EC2RouteTable -RouteTableId $rtTblId

# Create Subnet1 & associate route table (non-availability zone specific)
#$sn1Result = New-EC2Subnet -VpcId $vpcId -CidrBlock '10.20.1.0/28' -AvailabilityZone $availabilityZone
$sn1Result = New-EC2Subnet -VpcId $vpcId -CidrBlock '10.20.1.0/28'
$sn1Id = $sn1Result.SubnetId
Write-Output "Subnet1 ID : $sn1Id"
Register-EC2RouteTable -RouteTableId $rtTblId -SubnetId $sn1Id

<#
#Create Subnet2 & associate route table
$sn2Result = New-EC2Subnet -VpcId $vpcId -CidrBlock '10.20.2.0/28' -AvailabilityZone $availabilityZone
$sn2Id = $sn2Result.SubnetId
Write-Output "Subnet2 ID : $sn2Id"
Register-EC2RouteTable -RouteTableId $rtTblId -SubnetId $sn2Id
#>

Write-Output "VPC Setup Complete"
Get-EC2VPC -VpcId $vpcId

## Setup Security Group

# Create security group (EC2-VPC)
$securityGroupId = New-EC2SecurityGroup -GroupName $customer -Description "Security group for $customer"

# Add inbound rules
#Grant-EC2SecurityGroupIngress -GroupId $securityGroupId -IpPermissions @{IpProtocol = "tcp"; FromPort = 5985; ToPort = 5986; IpRanges = @("0.0.0.0/0")} # powershell
Grant-EC2SecurityGroupIngress -GroupId $securityGroupId -IpPermissions @{IpProtocol = "tcp"; FromPort = 22; ToPort = 22; IpRanges = @($whitelistIPs)}            # ssh
Grant-EC2SecurityGroupIngress -GroupId $securityGroupId -IpPermissions @{IpProtocol = "tcp"; FromPort = 80; ToPort = 80; IpRanges = @($whitelistIPs)}            # http
Grant-EC2SecurityGroupIngress -GroupId $securityGroupId -IpPermissions @{IpProtocol = "tcp"; FromPort = 443; ToPort = 443; IpRanges = @($whitelistIPs)}          # https

# Add outbound rules
#Grant-EC2SecurityGroupEgress -GroupId $securityGroupId -IpPermissions @{IpProtocol = "tcp"; FromPort = 5985; ToPort = 5986; IpRanges = @("0.0.0.0/0")}
Grant-EC2SecurityGroupEgress -GroupId $securityGroupId -IpPermissions @{IpProtocol = "tcp"; FromPort = 22; ToPort = 22; IpRanges = @($whitelistIPs)}            # ssh
Grant-EC2SecurityGroupEgress -GroupId $securityGroupId -IpPermissions @{IpProtocol = "tcp"; FromPort = 80; ToPort = 80; IpRanges = @($whitelistIPs)}            # http
Grant-EC2SecurityGroupEgress -GroupId $securityGroupId -IpPermissions @{IpProtocol = "tcp"; FromPort = 443; ToPort = 443; IpRanges = @($whitelistIPs)}          # https

Write-Output "Security Group Setup Complete"
Get-EC2SecurityGroup -GroupNames $customer

## Setup Network ACL

# Create network ACL
$networkAclId = New-EC2NetworkAcl -VpcId $vpcId

# Add ACL rules
New-EC2NetworkAclEntry -NetworkAclId $networkAclId -RuleNumber <Int32> -Egress <Boolean>  -Protocol "tcp" -PortRange_From 22 -PortRange_To 22 -CidrBlock $whitelistIPs -RuleAction <RuleAction>  -IcmpTypeCode_Code <Int32> -IcmpTypeCode_Type <Int32>

## Launch EC2 Instance

# Get imageId
# Amazon Linux AMI - HVM (SSD) EBS-Backed 64-bit - Asia Pacific Sydney
# Amazon Linux AMI 2017.03 was released on 2017-04-04
# ami-162c2575
$ami = Get-EC2Image -Region ap-southeast-2 -Owner amazon -Filters @(
    @{Name = "architecture"; Values = "x86_64"};
    @{Name = "description"; Values = "Amazon Linux AMI $year*x86_64*GP2"};
    @{Name = "image-id"; Values = "ami-162c2575"};
    @{Name = "root-device-type"; Values = "ebs"};
    @{Name = "virtualization-type"; Values = "hvm"}
)
$amiImageId = $ami.imageid

Write-Output "Selected AMI image id: $amiImageId"

# Create volumes
$bootVolume = New-Object Amazon.EC2.Model.EbsBlockDevice
$bootVolume.VolumeSize = 10
$bootVolume.VolumeType = 'gp2'
$bootVolume.DeleteTermination = $true
$deviceMapping = New-Object Amazon.EC2.Model.BlockDeviceMapping
$deviceMapping.DeviceName = '/dev/sda1'
$deviceMapping.Ebs = $bootVolume

$volume2 = New-Object Amazon.EC2.Model.EbsBlockDevice
$volume2.VolumeSize = 10
$volume2.VolumeType = 'gp2'
$volume2.Encrypted = $true
###############$volume2. = $customerKp
$volume2.DeleteTermination = $false
$deviceMapping2 = New-Object Amazon.EC2.Model.BlockDeviceMapping
$deviceMapping2.DeviceName = 'xvdf'
$deviceMapping2.Ebs = $volume2

# Create VM
$newVM = New-EC2Instance -ImageId $amiImageId -MinCount 1 -MaxCount 1 -InstanceType t2.micro `
-SecurityGroupId $securityGroupId -SubnetId $sn1Id -AvailabilityZone $availabilityZone -AssociatePublicIp $true `
-KeyName $customerKp `
-BlockDeviceMapping $deviceMapping, $deviceMapping2 -TagSpecification $Tags --dry-run

#$instanceId = $newVM.Instances[0].InstanceId
$instanceId = ($newVM.Instances).InstanceId
Write-Output "EC2 Instance launched"
Get-EC2Instance -InstanceId $instanceId

# Register IAM Instance Profile
#Register-EC2IamInstanceProfile -InstanceId $instanceId

# Create S3 bucket
#Random string generator to ensure bucket name is unique
#Courtesy https://blogs.technet.microsoft.com/heyscriptingguy/2015/11/05/generate-random-letters-with-powershell/
$randomString5 = -join ((65..90) + (97..122) | Get-Random -Count 5 | % {[char]$_})
$bucketName = $customer + "dashstorage-" + $randomString5
New-S3Bucket -BucketName $customer -Region ap-southeast-2
Write-S3BucketTagging -BucketName $bucketName -TagSet $Tags
Write-S3BucketVersioning -BucketName $bucketName -VersioningConfig_Status Enabled
#Write-S3LifecycleConfiguration -BucketName $bucketName -Configuration_Rule <LifecycleRule[]>

Write-Output ""
Write-Output "End of Script"
Write-Output ""
Write-Output "All good in the hood"
Write-Output ""
return 0